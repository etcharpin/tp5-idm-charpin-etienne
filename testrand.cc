#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>       
#include "CLHEP/Random/MTwistEngine.h"
#include "simuPi.c"


/**
 * @brief Generates random numbers and discards them.
 * @param nb_tirage Number of random numbers to generate.
 * @param s Pointer to CLHEP MTwistEngine.
 */

void tirer(int nb_tirage,CLHEP::MTwistEngine * s){

    double    f;
    
    for(int i = 1 ; i < nb_tirage ; i++){
      f = s-> flat();
      //printf("%f\n", f);
    }
  
}


/**
 * @brief Conducts multiple replications of random number generation and saves/restores states.
 * @param s Pointer to CLHEP MTwistEngine.
 * @param nb_file Number of files to save states.
 * @param nb_tirages Number of random numbers to generate in each replication.
 */

void question2(CLHEP::MTwistEngine * s, int nb_file , int nb_tirages){

  for(int i = 0; i < nb_file ; i++){
    tirer(nb_tirages,s);
    s->saveStatus(("./status" + std::to_string(i)).c_str());
    //printf("///////////////////////////////////////\n");
  } 
  s->restoreStatus("./status0");
  tirer(10,s);
  
} 

/**
 * @brief Conducts Pi simulation using Monte Carlo method and prints the results.
 * @param s Pointer to CLHEP MTwistEngine.
 */

void PiQuestion(CLHEP::MTwistEngine * s){
  double pi_,
         time_spent   = 0.0;

  clock_t begin       = clock(),
          end;

  int nb_replications = 10,
      nb_tirages      = 1000000000;


  for(int i = 0; i< nb_replications; i++){
    pi_ = simulPi(nb_tirages,s);
    printf("Pi = %f pour la replication numero %d\n",pi_,i );
  }

  end = clock();

  time_spent += (double)(end - begin) / CLOCKS_PER_SEC;
  printf("The elapsed time during Pi simulation is %f seconds\n", time_spent);
}




int main (){
  CLHEP::MTwistEngine * s = new CLHEP::MTwistEngine();

  ///////////////////Question 2///////////////////////////
  //question2(s,3,10);


  ///////////////////Question 3///////////////////////////
  //question2(s,10,2000000000);

  ///////////////////Question 4///////////////////////////
  PiQuestion(s);

  
  delete s;

  return 0;
}









