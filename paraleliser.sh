#!/bin/bash

fichier_entree=status
fichier_sortie_base=estimPi

nombre_executions=10

start_time=$(date +%s)

for ((i=0; i<$nombre_executions; i++)); do
	./SinglePi "${fichier_entree}$i" > "${fichier_sortie_base}$i" &
done

wait
end_time=$(date +%s)

total_time=$((end_time - start_time))

echo "Toutes les exécutions sont terminées en $total_time secondes."