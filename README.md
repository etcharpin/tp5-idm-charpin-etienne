# TP5-IDM Charpin Etienne



# Présentation

Repository du TP numéro 5 d'ingénierie des modeles et simulation de troisième année ISIMA

 - Le rapport ci-contre vous expliquera comment installer la librairie CLHEP et quels résultats nous en sortons.

 - Le fichier testrand.cc vous permettra de tester les questions 2, 3 et 4
 - Les fichiers simulPi.c et SinglePi.c vous permettra de lancer la question 4
 - Le script paraleliser.sh vous permettra de lancer la question 5 via la commande ./paraleliser.sh
 - Enfin le fichier pthread_test.c vous permettra de lancer la question 6.a