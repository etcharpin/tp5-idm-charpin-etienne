#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>  
#include <sys/time.h>
#include "CLHEP/Random/MTwistEngine.h"
#include "simuPi.c"

#define NUM_THREADS 10


/**
 * @brief Function executed by each thread.
 * @param thread_id The identifier of the thread.
 * @return Returns a pointer to the result, or NULL on thread exit.
 */

void *fonction_thread(void *thread_id) {

    CLHEP::MTwistEngine * s = new CLHEP::MTwistEngine();
    long tid;
    double pi;
    tid = (long)thread_id;
    //printf("Thread %ld : Démarrage...\n", tid);
    s->restoreStatus(("./status" + std::to_string(tid)).c_str());
    pi = simulPi(1000000000,s);
    //printf("Pi = %f\n",pi);

    delete s;

    //printf("Thread %ld : Terminé.\n", tid);
    pthread_exit(NULL);
}

/**
 * @brief Main function, entry point of the program.
 * @return Returns 0 on successful execution.
 */

int main() {
    
    pthread_t threads[NUM_THREADS];
    int erreur;
    long i;
    double time_spent = 0.0;
    struct timeval begin, end;
    gettimeofday(&begin, NULL);
    for (i = 0; i < NUM_THREADS; i++) {
        erreur = pthread_create(&threads[i], NULL, fonction_thread, (void *)i);
        if (erreur) {
            printf("Erreur : impossible de créer le thread %ld\n", i);
            exit(-1);
        }
    }

    // Attendre que tous les threads se terminent
    for (i = 0; i < NUM_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    gettimeofday(&end, NULL);

    time_spent = (end.tv_sec - begin.tv_sec) + (end.tv_usec - begin.tv_usec) / 1e6;
    printf("Tous les threads sont terminés en %f secondes.\n",time_spent);
    pthread_exit(NULL);

    return 0;
}
