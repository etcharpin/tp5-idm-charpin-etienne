
#include "CLHEP/Random/MTwistEngine.h"

// ----------------------------------------------------------------------- //
// simulPi               Estimation of PI with the Monte Carlo method      //
//                                                                         //
// Two points are drawns in the unit square [0..1]x[0..1]                  //
// Points falling on the Unit quarter of a Disk are counted                //
// The proportion of numbers falling on the disk divided by the total      //
// number drawns is slowly converging towards PI/4.                        //
// Much faster techniques exists but this approach has no equivalent in    //
// high dimensions.                                                        //
// Beware to compute with float numbers in double percision.               //
//                                                                         //
// Input : inNbPts the number of points to estimate PI                     //
//                                                                         //
// Output: a rough approximation of PI (draw 1 billion points to get at    //
//         least 4 exact digits.                                           //
// ----------------------------------------------------------------------- //

double simulPi(long inNbPts,CLHEP::MTwistEngine * s)
{
   long   i;
   double xr     = 0.;
   double yr     = 0.;
   double pi     = 0.;
   long   inDisk = 0;

   // Draw & test for inNbPts
   for(i = 0; i < inNbPts; i++)
   {
    // 2 drawings needed for a single (x,y) point in [O..1]
      xr = s-> flat();
      yr = s-> flat();
 
      // Check if the point is on the quarter of disk with radius = 1
      if ((xr * xr + yr * yr) <=1)
      {
         inDisk++;
      }
   }

   // Transtyping of intergers in double precison
   pi = ((double) inDisk / (double) inNbPts) * 4.;

   return pi;
}

// ----------------------------------------------------------------------- //