#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <string>
#include "CLHEP/Random/MTwistEngine.h"
#include "simuPi.c"

/**
 * @brief Restores the state of CLHEP MTwistEngine, simulates Pi, and prints the result.
 * @param argc Number of command-line arguments.
 * @param argv Array of command-line arguments.
 * @return Returns 0 on successful execution.
 */

int main (int argc, char ** argv){

  double pi;		
  CLHEP::MTwistEngine * s = new CLHEP::MTwistEngine();

  s->restoreStatus(argv[1]);

  pi = simulPi(1000000000,s);
  printf("Pi = %f\n",pi);

  
   delete s;

  return 0;
}